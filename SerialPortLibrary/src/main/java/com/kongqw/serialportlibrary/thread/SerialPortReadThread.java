package com.kongqw.serialportlibrary.thread;

import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;

import com.kongqw.serialportlibrary.util.BytesUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.CharsetEncoder;

/**
 * Created by Kongqw on 2017/11/14.
 * 串口消息读取线程
 */

public abstract class SerialPortReadThread extends Thread {

    public abstract void onDataReceived(byte[] bytes);

    private BufferedReader br = null;
    private static final String TAG = SerialPortReadThread.class.getSimpleName();
    private InputStream mInputStream;
    private byte[] mReadBuffer;

    public SerialPortReadThread(InputStream inputStream) {
        Log.i("gong", "启动串口服务线程" + Thread.currentThread().getName());
        mInputStream = inputStream;
        mReadBuffer = new byte[1024];
    }

    @Override
    public void run() {
        super.run();
        while (!isInterrupted()) {

            if (mInputStream == null) {
                return;
            }
            br = new BufferedReader(new InputStreamReader(mInputStream));
            byte[] readData = new byte[1024];
            String s = "";
            try {
                while ((s = br.readLine()) != null) {
                    if (TextUtils.isEmpty(s)) continue;
                    Log.d("gonggggggggggg" + Thread.currentThread().getName(), "000000000000" + s);
                    onDataReceived(s.getBytes());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public String bytesToHexString(byte[] bArr) {
        StringBuffer sb = new StringBuffer(bArr.length);
        String sTmp;

        for (int i = 0; i < bArr.length; i++) {
            sTmp = Integer.toHexString(0xFF & bArr[i]);
            if (sTmp.length() < 2)
                sb.append(0);
            sb.append(sTmp.toUpperCase());
        }

        return sb.toString();
    }

    @Override
    public synchronized void start() {
        super.start();
    }

    /**
     * 关闭线程 释放资源
     */
    public void release() {
        interrupt();

        if (null != mInputStream) {
            try {
                mInputStream.close();
                mInputStream = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
